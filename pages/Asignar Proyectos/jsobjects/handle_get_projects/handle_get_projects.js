export default {
	getProjects: async () => {
		if (tutor_filter.selectedOptionValue !== null && tutor_filter.selectedOptionValue !== undefined && tutor_filter.selectedOptionValue !== '') {
			await get_projects_with_tutor.run()
			return get_projects_with_tutor.data
		} else {
			await get_projects.run()
			return get_projects.data
		}
	},
	unassignTutor: () => {
		console.log(JSON.stringify(Projects.selectedRows.map(r => {
			return {
				...r,
				tutor_id: null,
				status: "IN_REVIEW"
			}}
		)))
	}
}
